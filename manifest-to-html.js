export function manifestToHtml (manifest) {
  if (typeof manifest === 'string') manifest = JSON.parse(manifest)
  var icons = ''
  var largestIcon
  var favicon
  if (manifest.icons) {
    //    icons += ``
    for (var i in manifest.icons) {
      if (manifest.icons[i].type === 'image/x-icon') {
        icons += `
<link rel="shortcut icon" href="${manifest.icons[i].src}">`
      } else if (manifest.icons[i].type === 'image/png') {
        icons += `
<link rel="icon" type="${manifest.icons[i].type || 'image/png'}" sizes="${manifest.icons[i].size}" href="${manifest.icons[i].src}">
<link rel="apple-touch-icon" type="${manifest.icons[i].type || 'image/png'}" sizes="${manifest.icons[i].size}" href="${manifest.icons[i].src}">`
      }
      if (largestIcon === undefined || Number(manifest.icons[i].sizes.split('x')[0]) > Number(largestIcon.sizes.split('x')[0])) largestIcon = manifest.icons[i]
    }
  }
  var loading = ''
  if (largestIcon) {
    loading = `<style>
      html {
        height: 100%;
      }
      body {
        height: 100%;
        background: ${manifest.background_color || ''} url(${largestIcon.src}) center center no-repeat;
      }
    </style>`
  }

  var links = ''
  if (manifest.links) {
    for (var l in manifest.links) {
      links += `
<link rel="${manifest.links[l].rel || 'stylesheet'}" type="${manifest.links[l].type || 'text/css'}" href="${manifest.links[l].src}"></link>`
    }
  }

  var scripts = ''
  if (manifest.scripts) {
    for (var s in manifest.scripts) {
      scripts += `
<script `
      if (manifest.scripts[s].type) scripts += ` type="${manifest.scripts[s].type}"`
      scripts += ` src="${manifest.scripts[s].src}"`
      if (manifest.scripts[s].async) scripts += ' async'
      scripts += `></script>`
    }
  }

  return `
<!doctype html>
<html lang="${manifest.lang || 'en'}">
  <head>
    <meta charset="${manifest.charset || 'UTF-8'}">

    <!-- generally good and probably true settings for a PWA -->
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <!-- taken from the manifest -->
    <title>${manifest.name}</title>
    <meta name="description" content="${manifest.description}">
    <meta name="application-name" content="${manifest.name}">
    <meta name="apple-mobile-web-app-title" content="${manifest.name}">
    <meta name="theme-color" content="${manifest.theme_color || 'white'}">
    <link rel="manifest" href="${manifest.scope || '/'}manifest.json">
    <link rel="canonical" href="https://${typeof(path) !== 'undefined' && path.env && path.env.HOSTNAME ? path.env.HOSTNAME : 'localhost'}${manifest.scope}" />
    <base href="${manifest.scope}">

    <!-- icons -->${icons}

    <!-- links -->${links}

    <!-- scripts -->${scripts}

  </head>
  <body>
    ${loading}
    <noscript>Please enable Javascript to use this web app.</noscript>
  </body>
</html>
`
}

export default manifestToHtml
